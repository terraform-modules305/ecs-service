# Module Ecs
This module help you to describe ECS-service to run container within EC2.

```
module "ecs_service" {
  source = "gitlab.itsupportme.by/terraform-templates/module-ecs-service/local"
  version = "0.0.1"
}
```

## Inputs variables

| Variable                           | Default value | Definition                                                                                                     | 
| ---------------------------------- | ------------- | -------------------------------------------------------------------------------------------------------------- |
| log_group                          | null          | Log group at CloudWatch                                                                                        |
| ecr_url                            | null          | URL of the Elastic Container Registry                                                                          |
| repository_name                    | null          | URL of the repository                                                                                          |
| COMMIT                             | latest        | Commit                                                                                                         |
| region                             | null          | Region                                                                                                         |
| container_name                     | null          | The task name which gives the name to the ECS task, container and service discovery name                       |
| execution_role_arn                 | null          | Task definition execution role                                                                                 |
| vpc_id                             | null          | VPC id of the ECS security group                                                                               |
| open_ports                         | null          | The ports which should be opened in the container and the security group to allow communication among services |
| cluster_name                       | null          | ECS-cluster name                                                                                               |
| desired_count                      | 1             | The desired number of the ECS task to run                                                                      |
| deployment_maximum_percent         | 100           | The maximum number of tasks which can run during redeployment of the service                                   |
| deployment_minimum_healthy_percent | 0             | The minimum percentage of running tasks to consider the service healthy                                        |
| target_group_arn                   | null          | The target group which used for ecs-service ALB                                                                |
| alb_port                           | null          | Port for the target group of application load balancer                                                         |
| has_alb                            | false         | If service has ALB                                                                                             |
| pid_mode                           | null          | The process namespace to use for the containers in the task (host for EC2)                                     |
| containerPort                      | null          | Port of container                                                                                              |
| wait_for_steady_state              | false         |                                                                                                                |
| cpu                                | 256           | CPU of container                                                                                               |
| memory                             | 512           | Memory of container                                                                                            |
| vpc_cidr                           | ["0.0.0.0/0"] | The trusted VPC CIDR to assign to the task security group ingress block                                        |
| has_discovery                      | false         | Flag to switch on service discovery. If true, a valid DNS namespace must be provided                           |
| dns_namespace                      | null          | The Route53 DNS namespace where the ECS task is registered                                                     |
| has_asg                            | false         | Flag to switch on service discovery. If true, a valid DNS namespace must be provided                           |
| capacity_provider_strategies       | null          | The capacity provider strategies to use for the service                                                        |