{
  "name": "${container_name}",
  "image": "${ecr_url}/${repository_name}:${COMMIT}@${image_digest}",
  "cpu": ${cpu},
  "memory": ${memory},
  "memoryReservation": ${memoryReservation},
  "logConfiguration": {
    "logDriver": "awslogs",
    "options": {
      "awslogs-group": "${log_group}",
      "awslogs-region": "${region}",
      "awslogs-create-group": "true",
      "awslogs-stream-prefix": "ecs"
    }
  },
  "links": [],
  "portMappings": [
      {
          "hostPort": 0,
          "containerPort": ${containerPort},
          "protocol": "tcp"
      }
  ],
  "essential": true,
  "entryPoint": [],
  "command": [],
  "mountPoints": [],
  "volumesFrom": []
}