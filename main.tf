# Updating task definition with new image every time
data "aws_ecr_image" "service_image" {
  repository_name = "${var.repository_name}"
  image_tag       = "${var.COMMIT}"
}

#========== Task definition ==========
data "template_file" "def" {
  template = file("${path.module}/task_definition.json.tpl")

  vars = {
    log_group           = "${var.log_group}"
    ecr_url             = "${var.ecr_url}"
    region              = "${var.region}"
    container_name      = "${var.container_name}"
    COMMIT              = "${var.COMMIT}"
    repository_name     = "${var.repository_name}"
    containerPort       = "${var.containerPort}"
    image_digest        = "${data.aws_ecr_image.service_image.image_digest}"
    cpu                 = "${var.cpu}"
    memory              = "${var.memory}"
    memoryReservation   = "${var.memoryReservation}"
  }
}

data "aws_ecr_repository" "service" {
  name = "${var.repository_name}"
}

resource "aws_ecs_task_definition" "task-def" {
  family                = "${var.container_name}-definition"
  container_definitions = format("[%s]", join(",", [data.template_file.def.rendered]))

  requires_compatibilities = ["EC2"]
  network_mode             = "bridge"
  execution_role_arn       = var.execution_role_arn
  cpu                      = var.cpu
  memory                   = var.memory
  pid_mode                 = var.pid_mode

  tags = {
    Name      = "${var.container_name}-definition"
    Terraform = "true"
  }
}

# ========= ECS security group ==========

resource "aws_security_group" "security_group" {

  description = "The security group of the ECS task allowing traffic only on the container ports"

  vpc_id = var.vpc_id

  dynamic "ingress" {
    for_each = var.open_ports
    content {
      protocol    = "tcp"
      from_port   = ingress.value
      to_port     = ingress.value
      cidr_blocks = var.vpc_cidr
    }
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = var.vpc_cidr
  }

  tags = {
    Name      = "${var.container_name}-sg"
    Terraform = "true"
  }
}

# ========= Service discovery ==========

resource "aws_service_discovery_service" "main" {

  count = var.has_discovery == false ? 0 : 1
  name  = var.container_name

  dns_config {
    namespace_id = var.dns_namespace

    dns_records {
      ttl  = 10
      type = "A"
    }

    routing_policy = "MULTIVALUE"
  }
}

resource "aws_ecs_service" "service" {
  name                               = var.container_name
  cluster                            = var.cluster_name
  task_definition                    = aws_ecs_task_definition.task-def.arn
  desired_count                      = var.desired_count
  launch_type                        = "EC2"
  deployment_maximum_percent         = var.deployment_maximum_percent
  deployment_minimum_healthy_percent = var.deployment_minimum_healthy_percent
  wait_for_steady_state              = var.wait_for_steady_state

  dynamic "service_registries" {
    for_each = var.has_discovery == false ? [] : tolist([var.has_discovery])
    content {
      registry_arn   = aws_service_discovery_service.main[0].arn
      container_name = var.container_name
    }
  }

  dynamic "capacity_provider_strategy" {
    for_each = var.has_asg == false ? [] : tolist([var.has_asg])
    content {
      capacity_provider = var.capacity_provider
      weight            = 1
    }
  }

  dynamic "capacity_provider_strategy" {
    for_each = var.capacity_provider_strategies
    content {
      capacity_provider = capacity_provider_strategy.value.capacity_provider
      weight            = capacity_provider_strategy.value.weight
      base              = lookup(capacity_provider_strategy.value, "base", null)
    }
  }

  dynamic "load_balancer" {
    for_each = var.has_alb == false ? [] : tolist([var.has_alb])
    content {
      container_name   = var.container_name
      target_group_arn = var.target_group_arn
      container_port   = var.alb_port
    }
  }

  ordered_placement_strategy {
    type  = "binpack"
    field = "cpu"
  }

  tags = {
    Name      = "${var.container_name}-service"
    Terraform = "true"
  }
}