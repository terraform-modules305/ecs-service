variable "log_group" {
  description = "Log group at CloudWatch"
  type        = string
  default     = null
}

variable "ecr_url" {
  description = "URL of the Elastic Container Registry"
  type        = string
  default     = null
}

variable "repository_name" {
  description = "URL of the repository"
  type        = string
  default     = null
}

variable "COMMIT" {
  type    = string
  default = "latest"
}

variable "region" {
  type    = string
  default = null
}

variable "container_name" {
  description = "The task name which gives the name to the ECS task, container and service discovery name"
  type        = string
  default     = null
}

variable "execution_role_arn" {
  description = "Task definition execution role"
  type        = string
  default     = null
}

variable "vpc_id" {
  description = "VPC id of the ECS security group"
  type        = string
  default     = null
}

variable "open_ports" {
  description = "The ports which should be opened in the container and the security group to allow communication among services"
  type        = list(string)
  default     = null
}

variable "cluster_name" {
  description = "ECS-cluster name"
  type        = string
  default     = null
}

variable "desired_count" {
  description = "The desired number of the ECS task to run"
  type        = number
  default     = 1
}

variable "deployment_maximum_percent" {
  description = "The maximum number of tasks which can run during redeployment of the service"
  type        = number
  default     = 100
}

variable "deployment_minimum_healthy_percent" {
  description = "The minimum percentage of running tasks to consider the service healthy"
  type        = number
  default     = 0
}

variable "target_group_arn" {
  description = "The target group which used for ecs-service ALB"
  type        = string
  default     = null
}

variable "alb_port" {
  description = "Port for the target group of application load balancer"
  type        = number
  default     = null
}

variable "has_alb" {
  description = "If service has ALB"
  type        = bool
  default     = false
}

variable "pid_mode" {
  description = "The process namespace to use for the containers in the task (host for EC2)"
  default     = null
}

variable "containerPort" {
  description = "Port of container"
  type        = number
  default     = null
}

variable "wait_for_steady_state" {
  type    = bool
  default = false
}

variable "cpu" {
  description = "CPU of container"
  type        = number
  default     = 256
}

variable "memory" {
  description = "Memory of container"
  type        = number
  default     = 512
}

variable "memoryReservation" {
  description = "Memory of container (soft limit)"
  type        = number
}

variable "vpc_cidr" {
  description = "The trusted VPC CIDR to assign to the task security group ingress block"
  type        = list(string)
  default     = ["0.0.0.0/0"]
}

variable "has_discovery" {
  description = "Flag to switch on service discovery. If true, a valid DNS namespace must be provided"
  type        = bool
  default     = false
}

variable "dns_namespace" {
  description = "The Route53 DNS namespace where the ECS task is registered"
  type        = string
  default     = null
}

variable "has_asg" {
  description = "Flag to switch on service discovery. If true, a valid DNS namespace must be provided"
  type        = bool
  default     = false
}

variable "capacity_provider_strategies" {
  type = list(object({
    capacity_provider = string
    weight            = number
    base              = number
  }))
  description = "The capacity provider strategies to use for the service. See `capacity_provider_strategy` configuration block: https://www.terraform.io/docs/providers/aws/r/ecs_service.html#capacity_provider_strategy"
  default     = []
}